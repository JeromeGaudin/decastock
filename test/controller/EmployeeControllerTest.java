package controller;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import model.Database;
import model.EmployeeAccount;
import model.Shelf;

public class EmployeeControllerTest {
	private Database db;
	private EmployeeAccount empToto;
	
	@Before
	public void initialization() {
		db = new Database();
		Shelf shelf = new Shelf("bike");
		empToto = new EmployeeAccount("toto", "tata", "toto", "toto", shelf);
	}
	
	@Test
	public void addEmployee1() {
		EmployeeController employeeController = new EmployeeController();
		
		boolean b = employeeController.addEmployee(empToto, db);
		assertEquals(true, b);
	}
	
	@Test
	public void addEmployee2() {
		EmployeeController employeeController = new EmployeeController();
		
		boolean b = employeeController.addEmployee(empToto, db);
		assertEquals(true, b);
		b = employeeController.addEmployee(empToto, db);
		assertEquals(false, b);
	}
}
