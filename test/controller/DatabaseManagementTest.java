package controller;

import java.io.FileNotFoundException;

import org.junit.Test;

public class DatabaseManagementTest {
	
	@Test(expected = FileNotFoundException.class)
	public void loadDataBaseThrowException() {
		DatabaseManagement dbm = new DatabaseManagement();
		dbm.loadDatabase("inexistantFile");
	}
	
	@Test
	public void testStoreData() {
		//TODO one test or more
	}
	
	@Test
	public void testLoadData() {
		//TODO one test or more
	}
}
