package model;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class AccountTest {
	private Account account;
	
	@Before
	public void initialize() {
		account = new Account("Jean", "Bon", "login", "passwd");
	}
	
	@Test
	public void passwordAuthorize() {
		boolean test = account.passwordAuthorize("");
		assertEquals(false,test);
	}
	
	@Test
	public void passwordAuthorize2() {
		boolean test = account.passwordAuthorize("toto2");
		assertEquals(true,test);
	}
	
	@Test
	public void passwordAuthorize3() {
		boolean test = account.passwordAuthorize("toto!");
		assertEquals(false,test);
	}
	
	@Test
	public void passwordAuthorize4() {
		boolean test = account.passwordAuthorize("Mélanie");
		assertEquals(false,test);
	}
	@Test
	public void passwordAuthorize5() {
		boolean test = account.passwordAuthorize("melanie");
		assertEquals(true,test);
	}
	
	@Test
	public void passwordAuthorize6() {
		boolean test = account.passwordAuthorize("MELANIE");
		assertEquals(true,test);
	}
	
	@Test
	public void paramAuthorize7() {
		boolean test = account.passwordAuthorize("cMelanie");
		assertEquals(true,test);
	}
	
	@Test
	public void setPassword0() {
		account.setPassword("toto");
		assertEquals("toto",account.getPassword());
	}

	@Test
	public void setPassword1() {
		account.setPassword("toTo9118");
		assertEquals("toTo9118",account.getPassword());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setPassword2() {
		account.setPassword("tOtO§");
	}
	
	
	@Test
	public void nameAuthorize() {
		boolean test = account.nameAuthorize("");
		assertEquals(false,test);
	}
	
	@Test
	public void nameAuthorize2() {
		boolean test = account.nameAuthorize("hello");
		assertEquals(true,test);
	}
	
	@Test
	public void nameAuthorize3() {
		boolean test = account.nameAuthorize("c'est bibi!");
		assertEquals(false,test);
	}
	
	@Test
	public void nameAuthorize4() {
		boolean test = account.nameAuthorize("Jean");
		assertEquals(true,test);
	}
	@Test
	public void nameAuthorize5() {
		boolean test = account.nameAuthorize("JeAn");
		assertEquals(true,test);
	}
	
	
	@Test
	public void setName() {
		account.setFirstName("toto");
		assertEquals("toto",account.getFirstName());
	}

	@Test(expected = IllegalArgumentException.class)
	public void setName1() {
		account.setFirstName("toTo98");
		assertEquals("toTo98",account.getFirstName());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setName2() {
		account.setFirstName("tOtO§");
	}
	
	//test getters
	@Test
	public void isAccess() {
		assertEquals(false,account.isAccess());
	}

	@Test
	public void getLogin() {
		assertEquals("login",account.getLogin());
	}
	
	@Test
	public void getPassword() {
		assertEquals("passwd",account.getPassword());
	}
	
	@Test
	public void getLastName() {
		assertEquals("Bon",account.getLastName());
	}
	
	@Test
	public void getFirstName() {
		assertEquals("Jean",account.getFirstName());
	}

	//test setters	
	@Test
	public void setAccess() {
		account.setAccess(true);
		assertEquals(true,account.isAccess());
	}
	
	@Test
	public void setLastName() {
		account.setLastName("LeBon");
		assertEquals("LeBon",account.getLastName());
	}
	
	@Test
	public void setFirstName() {
		account.setFirstName("Toto");
		assertEquals("Toto",account.getFirstName());
	}
	
}