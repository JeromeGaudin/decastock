package model;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class ShelfTest {
	private Shelf shelf;
	
	@Before
	public void initialize() {
		shelf = new Shelf("monRayon");
	}
	
	//test getters
	@Test
	public void getName() {
		assertEquals("monRayon",shelf.getName());
	}
	
	@Test
	public void getProduts() {
		List<Product> list = new ArrayList<>();
		assertEquals(list,shelf.getProduts());
	}
	
	@Test
	public void getEmployees() {
		List<EmployeeAccount> list = new ArrayList<>();
		assertEquals(list,shelf.getEmployees());
	}
	
	//test setters
	@Test
	public void setName() {
		shelf.setName("Sport");
		assertEquals("Sport",shelf.getName());
	}
	
	@Test
	public void setProduts() {
		List<Product> list = new ArrayList<>();
		
		assertEquals(list,shelf.getProduts());
	}
	
	@Test
	public void setEmployees() {
		List<EmployeeAccount> list = new ArrayList<>();
		
		assertEquals(list,shelf.getEmployees());
	}
}
