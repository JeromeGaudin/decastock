package model;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ProductTest {
	private Product product;
	private Shelf shelf;
	
	@Before
	public void initialize() {
		shelf = new Shelf("monRayon");
		product = new Product("chaussures", 40.5, 0.2, 4, shelf);
	}
	
	//tests getters
	@Test
	public void getName() {
		assertEquals("chaussures",product.getName());
	}
	
	@Test
	public void getPrice() {
		assertEquals(40.5,product.getPrice(),0.0001);
	}

	@Test
	public void getTVA() {
		assertEquals(0.2,product.getTVA(),0.0001);
	}
		
	@Test
	public void getQuantity() {
		assertEquals(4,product.getQuantity());
	}
	
	@Test
	public void getShelf() {
		assertEquals(shelf,product.getShelf());
	}
	
	//tests setters
	@Test
	public void setName0() {
		product.setName("ballon");
		assertEquals("ballon",product.getName());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setName1() {
		product.setName("");
	}
	
	@Test
	public void setName2() {
		product.setName("ballon2");
		assertEquals("ballon",product.getName());
	}
	
	@Test
	public void setPrice0() {
		product.setPrice(22.33);
		assertEquals(22.33,product.getPrice(),0.0001);
	}
	
	@Test
	public void setPrice1() {
		product.setPrice(0);
		assertEquals(0,product.getPrice(),0.0001);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setPrice2() {
		product.setPrice(-0.5);
	}
	
	@Test
	public void setTVA0() {
		product.setTVA(0.4);
		assertEquals(0.4,product.getTVA(),0.0001);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setTVA1() {
		product.setTVA(0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setTVA2() {
		product.setTVA(-0.4);
	}
	
	@Test
	public void setQuantity0() {
		product.setQuantity(7);
		assertEquals(7,product.getQuantity());
	}
	
	@Test
	public void setQuantity00() {
		product.setQuantity(0);
		assertEquals(0,product.getQuantity());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setQuantity1() {
		product.setQuantity(-7);
	}
	
	@Test
	public void setShelf0() {
		Shelf shelf2 = new Shelf("monAutreRayon");
		assertEquals(shelf2,product.getShelf());
	}
	
	@Test(expected = NullPointerException.class)
	public void setShelf1() {
		product.setShelf(null);
	}
	/**
	mockito
	objet
	**/
}
