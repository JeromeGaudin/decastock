package model;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class EmployeeAccountTest {
	private EmployeeAccount employee;
	private Shelf shelf;
	
	/**
	 * mock ! test
	 */
	
	
	@Before
	public void initialize() {
		shelf = new Shelf("monRayon");
		employee = new EmployeeAccount("Jean", "Naymar", "login", "passwd", shelf);
	}
	
	@Test
	public void getShelf() {
		assertEquals(shelf, employee.getShelf());
	}
	
	@Test
	public void setShelf() {
		Shelf shelf2 = new Shelf("unAutreRayon");
		employee.setShelf(shelf2);
		assertEquals(shelf2, employee.getShelf());
	}
	
}
