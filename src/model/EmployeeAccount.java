package model;

import java.io.Serializable;

public class EmployeeAccount extends Account  implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/**
	 * shelf is the shelf for which EmployeeAccount is responsible.
	 * shelf could be null
	 */
	private Shelf shelf;
	
	/**
	 * Constructor
	 * @param firstName
	 * @param lastName
	 * @param newLogin
	 * @param password
	 * @param shelf
	 */
	public EmployeeAccount(String firstName, String lastName, String newLogin, String password, Shelf shelf) {
		super(firstName, lastName, newLogin, password);
		setShelf(shelf);
		
	}
	
	/**
	 * Default Constructor do nothing
	 */
	public EmployeeAccount() {
		super();
	}
	
	//getter and setter
	public Shelf getShelf() {
		return shelf;
	}
	
	public void setShelf(Shelf shelf) {
		this.shelf = shelf;
	}	
}