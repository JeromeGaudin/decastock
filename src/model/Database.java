package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
public class Database implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * Attributes
	 */
	/**
	 * shelves attribute is the list of shelves in the store.
	 */
	private List<Shelf> shelves;
	/**
	 * products attribute is the list of products in the store.
	 */
	private List<Product> products;
	/**
	 * employees attribute is the list of employees in the store, without administrators.
	 */
	private List<EmployeeAccount> employees;
	/**
	 * administrator attribute is the list of administrators in the store.
	 */
	private List<AdminAccount> admins;
	
	/**
	 * Default constructor
	 */
	public Database() {
		shelves = new ArrayList<>();
		products = new ArrayList<>();
		employees = new ArrayList<>();
		admins = new ArrayList<>();
	}
	
	//add and remove methods
	public void addShelf(Shelf shelf) {
		shelves.add(shelf);
	}
	public void removeShelf(Shelf shelf) {
		shelves.remove(shelf);
	}
	public void addProduct(Product product) {
		products.add(product);
	}
	public void removeProduct(Product product) {
		products.remove(product);
	}
	public void addEmployee(EmployeeAccount employee) {
		employees.add(employee);
	}
	public void removeEmployee(EmployeeAccount employee) {
		employees.remove(employee);
	}
	public void addAdmin(AdminAccount admin) {
		admins.add(admin);
	}
	public void removeAdmin(AdminAccount admin) {
		admins.remove(admin);
	}

	//getters and setters
	//setters are called after the de-serialization
	public List<Shelf> getShelves() {
		return shelves;
	}
	public void setShelves(List<Shelf> shelves) {
		this.shelves = shelves;
	}
	public List<Product> getProducts() {
		return products;
	}
	public void setProducts(List<Product> products) {
		this.products = products;
	}
	public List<EmployeeAccount> getEmployees() {
		return employees;
	}
	public void setEmployees(List<EmployeeAccount> employees) {
		this.employees = employees;
	}
	public List<AdminAccount> getAdmins() {
		return admins;
	}
	public void setAdmins(List<AdminAccount> admins) {
		this.admins = admins;
	}
}
