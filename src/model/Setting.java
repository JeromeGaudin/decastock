package model;

/**
 * class to save all setting of this application
 */
public class Setting {
	/**
	 * Constant for save the path of database
	 */
	public static final String BACKUP_FILE_PATH = "database";
	
	private Setting() {
		// nothing
	}
}
