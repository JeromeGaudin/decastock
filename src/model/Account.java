package model;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Account implements Serializable {
	/**
	 * login attribute is the login of the account. The login allow to recognize the account. 
	 * Login is unique as an ID.
	 */
	private String login;
	/**
	 * password attribute is the password of the account.
	 */
	private String password;
	/**
	 * firstName is the first name of the Account user
	 */
	private String firstName;
	/**
	 * lastName is the last name of the Account user
	 */
	private String lastName;
	/**
	 * access indicate if the account has the right to access to the application.
	 */
	private boolean access;
	
	/**
	 * Constructor
	 * @param newLogin
	 * @param password
	 */
	public Account(String firstName, String lastName, String newLogin, String password) {
		if ( !passwordAuthorize(newLogin)) {
			throw new IllegalArgumentException("Le champ Identifiant ne peut contenir que des lettres majuscules, minuscules ainsi que des chiffres");
		}
		if (newLogin.equals("")) {
			throw new IllegalArgumentException("Veuillez-remplir le champ Identifiant");
		}
		access = false;
		login = newLogin;
		setPassword(password);
		setLastName(lastName);
		setFirstName(firstName);
	}
	
	/**
	 * Default Constructor
	 */
	public Account () {
	}

	/**
	 * permit to see if param match with regex: ^[a-zA-Z]*$
	 * @param param
	 * @return True if param contains only letters
	 */
	public boolean nameAuthorize(String param) {
		if (!param.equals("")) {
			Pattern pattern = Pattern.compile("^[a-zA-Z]*$");
	        Matcher matcher = pattern.matcher(param);
	        if(matcher.find()) {
	        	return true;
	        }
		}
		return false;
	}
	
	/**
	 * permit to see if param match with regex: ^[a-zA-Z0-9]*$
	 * @param param 
	 * @return True if param contains only letters or numbers
	 */
	public boolean passwordAuthorize(String param) {
		if (!param.equals("")) {
			Pattern pattern = Pattern.compile("^[a-zA-Z0-9]*$");
	        Matcher matcher = pattern.matcher(param);
	        if(matcher.find()) {
	        	return true;
	        }
		}
		return false;
	}
	
	/**
	 * toString useful for JComboBox
	 */
	public String toString() {
		return firstName + " " + lastName;
	}
	
	//getters 
	public String getLogin() {
		return login;
	}

	public String getPassword() {
		return password;
	}
	
	public boolean isAccess() {
		return access;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	//setters
	public void setPassword(String password) {
		if (passwordAuthorize(password)) {
			this.password = password;
		} else {
			throw new IllegalArgumentException("Le champ mot de passe ne peut contenir que des lettres majuscules, minuscules ainsi que des chiffres");
		}
	}
	
	public void setAccess(boolean access) {
		this.access = access;
	}

	public void setFirstName(String firstName) {
		if ( !nameAuthorize(firstName)) {
			throw new IllegalArgumentException("Le champ Prénom ne peut contenir que des lettres majuscules et minuscules");
		}
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		if ( !nameAuthorize(lastName)) {
			throw new IllegalArgumentException("Le champ Nom ne peut contenir que des lettres majuscules et minuscules");
		}
		this.lastName = lastName;
	}
}
