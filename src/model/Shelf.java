package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Shelf  implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/**
	 * name attribute is the name of the shelf.
	 */
	private String name;
	/**
	 * products is the list of Product in the shelf.
	 */
	private List<Product> products;
	/**
	 * employees is the list of EmployeeAccount which are responsible of the shelf.
	 */
	private List<EmployeeAccount> employees;
	
	/**
	 * Constructor
	 * @param name
	 */
	public Shelf(String name) {
		setName(name);
		products = new ArrayList<>();
		employees = new ArrayList<>();
	}

	// add and remove methods
	public void addProduct(Product product) {
		products.add(product);
	}
	
	public void removeProduct(Product product) {
		products.remove(product);
	}
	
	public void addEmployee(EmployeeAccount employee) {
		employees.add(employee);
	}

	public void removeEmployee(EmployeeAccount employee) {
		employees.remove(employee);
	}
	
	/**
	 * permit to see if param match with regex: ^[a-zA-Z]*$
	 * @param param
	 * @return True if param contains only letters
	 */
	public boolean nameAuthorize(String param) {
		if (!param.equals("")) {
			Pattern pattern = Pattern.compile("^[a-zA-Z]*$");
	        Matcher matcher = pattern.matcher(param);
	        if(matcher.find()) {
	        	return true;
	        }
		}
		return false;
	}
	
	/**
	 * useful for JComboBox
	 */
	public String toString() {
		return name;
	}
	
	
	// setters and getters
	public String getName() {
		return name;
	}

	public void setName(String name) {
		if ( !nameAuthorize(name)) {
			throw new IllegalArgumentException("Le champ Nom ne peut contenir que des lettres majuscule et minuscule");
		}
		this.name = name;
	}

	public List<Product> getProduts() {
		return products;
	}
	
	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public List<EmployeeAccount> getEmployees() {
		return employees;
	}

	public void setEmployees(List<EmployeeAccount> employees) {
		this.employees = employees;
	}
}