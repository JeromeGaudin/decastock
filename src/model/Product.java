package model;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Product implements Serializable  {
	private static final long serialVersionUID = 1L;

	/**
	 * Attributes to describe a product
	 */
	private String name;
	/**
	 * price must be a positive value.
	 */
	private double price;
	/**
	 * TVA must be a positive value.
	 */
	private double tVA;
	/**
	 * quantity must be a positive value.
	 */
	private int quantity;
	/**
	 * shelf can not be null.
	 */
	private Shelf shelf;
	
	/**
	 * Constructor
	 * @param name
	 * @param price
	 * @param tVA
	 * @param quantity
	 * @param shelf
	 */
	public Product(String name, double price, double tVA, int quantity, Shelf shelf) {
		setName(name);
		setPrice(price);
		setTVA(tVA);
		setQuantity(quantity);
		setShelf(shelf);
	}
	
	/**
	 * nameAuthorize permit to see if param match with regex: ^[a-zA-Z]*$
	 * @param param
	 * @return True if param contains only letters
	 */
	public boolean nameAuthorize(String param) {
		if (!param.equals("")) {
			Pattern pattern = Pattern.compile("^[a-zA-Z]*$");
	        Matcher matcher = pattern.matcher(param);
	        if(matcher.find()) {
	        	return true;
	        }
		}
		return false;
	}
	
	
	// setters and getters
	public String getName() {
		return name;
	}
	
	/**
	 * set the name
	 * @param name to edit
	 * @throws IllegalArgumentException if name don't match with pattern of nameAuthorize() method
	 */
	public void setName(String name) {
		if ( !nameAuthorize(name)) {
			throw new IllegalArgumentException("Le champ Nom ne peut contenir que des lettres majuscules et minuscules");
		}
		if (!name.equals("")) {
			this.name = name;
		}else {
			throw new IllegalArgumentException();
		}
	}
	
	public double getPrice() {
		return price;
	}
	
	/**
	 * set the price
	 * @param price to edit
	 * @throws IllegalArgumentException if price are negative
	 */
	public void setPrice(double price) {
		if (price>=0) {
			this.price = price;
		} else {
			throw new IllegalArgumentException();
		}
	}
	
	public double getTVA() {
		return tVA;
	}
	
	/**
	 * set the TVA
	 * @param tVA to edit
	 * @throws IllegalArgumentException if tVA are negative
	 */
	public void setTVA(double tVA) {
		if (tVA>0) {
			this.tVA = tVA;
		} else {
			throw new IllegalArgumentException();
		}
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	/**
	 * set the quantity
	 * @param quantity to edit
	 * @throws IllegalArgumentException if the quantity are negative
	 */
	public void setQuantity(int quantity) {
		if (quantity>=0) {
			this.quantity = quantity;
		}else {
			throw new IllegalArgumentException();
		}
	}
	
	public Shelf getShelf() {
		return shelf;
	}
	
	/**
	 * set the shelf, all product must have a shelf
	 * @param shelf to edit
	 * @throws IllegalArgumentException if shelf are null
	 */
	public void setShelf(Shelf shelf) {
		if (shelf != null) {
			this.shelf = shelf;
		}else {
			throw new NullPointerException();
		}
	}
}