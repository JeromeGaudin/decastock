package model;

import java.io.Serializable;

public class AdminAccount extends EmployeeAccount implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor
	 * @param firstName of admin
	 * @param lastName of admin
	 * @param newLogin of admin
	 * @param password of admin
	 * @param shelf not significant
	 */
	public AdminAccount(String firstName, String lastName, String newLogin, String password, Shelf shelf) {
		super(firstName, lastName, newLogin, password, shelf);
	}

}
