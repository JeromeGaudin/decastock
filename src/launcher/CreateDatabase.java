package launcher;

import java.util.ArrayList;
import java.util.List;

import controller.DatabaseManagement;
import model.AdminAccount;
import model.Database;
import model.EmployeeAccount;
import model.Setting;
import model.Shelf;

/**
 * launcher to create a database with an admin account 
 * the admin account have as login "admin" and as password "admin"
 */
public class CreateDatabase {
	/**
	 * Main method to launch application
	 * @param args nothing, no parameter
	 */
	public static void main(String[] args) {
		DatabaseManagement dm = new DatabaseManagement();
		Database db = new Database();
		
		AdminAccount admin = new AdminAccount("Big", "Boss", "admin", "admin", null);
		admin.setAccess(true);
		
		List<AdminAccount> admins = new ArrayList<>();
		admins.add(admin);
		db.setAdmins(admins);
		
		dm.saveDatabase(Setting.BACKUP_FILE_PATH, db);
	}
}
