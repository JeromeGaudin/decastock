package launcher;

import controller.DatabaseManagement;
import model.Database;
import model.Setting;
import view.AppWindow;
import view.Connection;

public class App {
	/**
	 * Main method to launch application
	 * @param args nothing, no parameter
	 */
	public static void main(String[] args) {
		Connection connetion = new Connection();
		connetion.setVisible(true);
	}
}
