package view.component;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import model.Shelf;

/**
 * to see all shelf in a JTable
 */
public class SelectShelfTableModel extends AbstractTableModel {
	private static final long serialVersionUID = 1L;

	/**
	 * List of all shelf in the table
	 */
	private List<Shelf> shelfs;
	
	/**
	 * Constructor
	 */
	public SelectShelfTableModel() {
		shelfs = new ArrayList<>();
	}
	
	/**
	 * This method allows to edit shelfs attribute.
	 * @param data new shelf
	 */
	public void setData(List<Shelf> data) {
		shelfs = data;
	}
	
	/**
	 * get Shelf object at index i
	 * @param i index
	 * @return Shelf object at index i
	 */
	public Shelf getData(int i) {
		return shelfs.get(i);
	}
	
	/**
	 * remove a row
	 * @param i index
	 */
	public void removeRow(int i) {
		shelfs.remove(i);
	}

	/**
	 * This method returns the number of column.
	 */
	@Override
	public int getColumnCount() {
		return 2;
	}

	/**
	 * This method returns the number of row.
	 */
	@Override
	public int getRowCount() {
		return shelfs.size();
	}
	
	/**
	 * This method returns the name of column.
	 * @param columnIndex
	 */
    @Override
    public String getColumnName(int columnIndex) {
    	switch (columnIndex) {
    	case 0:
    		return "Rayon";
    	case 1:
    		return "Détail";
    	default:
    		return null;
    	}
    }
    
    /**
     * This method allow to modify row or not, useful for column of button
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
    	switch (columnIndex) {
		case 1:
			return true;
		default:
			return false;
    	}
    }

    /**
     * This method returns value in tab.
	 * @param columnIndex
	 * @param rowIndex
     */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Shelf c = shelfs.get(rowIndex);
		switch (columnIndex) {
		case 0:
			return c.getName();
		case 1:
			return "voir";
		default :
			return null;
		}
	}
}