package view.component;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import model.EmployeeAccount;

/**
 * to see many account in one table
 */
public class EmployeeAccountTableModel extends AbstractTableModel {
	private static final long serialVersionUID = 1L;

	/**
	 * list of all account in the table
	 */
	private List<EmployeeAccount> accounts;
	
	/**
	 * Constructor
	 */
	public EmployeeAccountTableModel() {
		accounts = new ArrayList<>();
	}
	
	/**
	 * This method allows to edit products attribute.
	 * @param data new shelf
	 */
	public void setData(List<EmployeeAccount> data) {
		accounts = data;
	}
	
	/**
	 * get Shelf object at index i
	 * @param i index
	 * @return Shelf object at index i
	 */
	public EmployeeAccount getData(int i) {
		return accounts.get(i);
	}
	
	/**
	 * remove a row
	 * @param i index
	 */
	public void removeRow(int i) {
		accounts.remove(i);
	}

	/**
	 * This method returns the number of column.
	 */
	@Override
	public int getColumnCount() {
		return 3;
	}

	/**
	 * This method returns the number of row.
	 */
	@Override
	public int getRowCount() {
		return accounts.size();
	}
	
	/**
	 * This method returns the name of column.
	 * @param columnIndex
	 */
    @Override
    public String getColumnName(int columnIndex) {
    	switch (columnIndex) {
    	case 0:
    		return "Compte";
    	case 1:
    	case 2:
    		return "";
    	default:
    		return null;
    	}
    }
    
    /**
     * This method allow to modify row or not, useful for column of button
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
    	switch (columnIndex) {
		case 1:
		case 2:
			return true;
		default:
			return false;
    	}
    }

    /**
     * This method returns value in tab.
	 * @param columnIndex
	 * @param rowIndex
     */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		EmployeeAccount a = accounts.get(rowIndex);
		switch (columnIndex) {
		case 0:
			return a.getLogin();
		case 1:
			return "Modifier";
		case 2:
			return "Supprimer";
		default :
			return null;
		}
	}
}
