package view.component;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import model.Product;
import model.Shelf;

/**
 * To see all product of one shelf in a JTable
 * There are also a column of button for edit a product
 */
public class SomeProductEditableTableModel extends AbstractTableModel {
	private static final long serialVersionUID = 1L;
	
	/**
	 * All product in the JTable
	 */
	private List<Product> products;
	
	/**
	 * shelf associate to all product
	 */
	private Shelf shelf;
	
	/**
	 * Constructor
	 */
	public SomeProductEditableTableModel(Shelf newShelf) {
		products = new ArrayList<>();
		shelf = newShelf;
	}
	
	/**
	 * This method allows to edit products attribute.
	 * @param data new shelf
	 */
	public void setData(List<Product> data) {
		products = data;
	}
	
	/**
	 * get Shelf object at index i
	 * @param i index
	 * @return Shelf object at index i
	 */
	public Product getData(int i) {
		return products.get(i);
	}
	
	/**
	 * remove a row
	 * @param i index
	 */
	public void removeRow(int i) {
		products.remove(i);
	}

	/**
	 * This method returns the number of column.
	 */
	@Override
	public int getColumnCount() {
		return 6;
	}

	/**
	 * This method returns the number of row.
	 */
	@Override
	public int getRowCount() {
		return products.size();
	}
	
	/**
	 * This method returns the name of column.
	 * @param columnIndex
	 */
    @Override
    public String getColumnName(int columnIndex) {
    	switch (columnIndex) {
    	case 0:
    		return "Produit";
    	case 1:
    		return "Supprimer";
    	case 2:
    		return "Quantité";
    	case 3:
    		return "Ajouter";
    	case 4:
    		return "Détail";
    	case 5:
    		return "Modifier";
    	default:
    		return null;
    	}
    }
    
    
    /**
     * This method allow to update quantity
     */
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    	super.setValueAt(aValue, rowIndex, columnIndex);
    	if (columnIndex == 2) {
    		products.get(rowIndex).setQuantity((int) aValue); 
    	}
    }
    
    /**
     * This method allow to modify row or not, useful for column of button
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
    	if (products.get(rowIndex).getShelf() == shelf) {
	    	switch (columnIndex) {
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
				return true;
			default:
				return false;
	    	}
    	}
    	return false;
    }

    /**
     * This method returns value in tab.
	 * @param columnIndex
	 * @param rowIndex
     */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Product p = products.get(rowIndex);
		switch (columnIndex) {
		case 0:
			return p.getName();
		case 1:
			return "-";
		case 2:
			return p.getQuantity();
		case 3:
			return "+";
		case 4:
			return "voir";
		case 5:
			return "modifier";
		default :
			return null;
		}
	}
}
