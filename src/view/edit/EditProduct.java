package view.edit;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import model.Database;
import model.Product;
import model.Shelf;
import view.AppWindow;
import view.View;

/**
 * window to edit a product
 */
public class EditProduct extends JFrame implements ActionListener {
	
	private JTextField textFieldName;
	private JSpinner spinnerPrice;
	private JSpinner spinnerTVA;
	private JSpinner spinnerQuantity;
	private JComboBox<Object> comboShelf;
	private JLabel lblError;
	
	private Product product;
	private View view;

	/**
	 * constructor
	 * @param newView the view to update after editing product
	 * @param product to edit
	 * @param database where all data are stored
	 */
	public EditProduct(View newView, Product product, Database database) {
		this.product = product;
		view = newView;
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(6,2));
		
		JLabel lblName = new JLabel("Nom");
		contentPane.add(lblName);
		
		JPanel panel1 = new JPanel();
		contentPane.add(panel1);
		textFieldName = new JTextField(product.getName());
		textFieldName.setColumns(10);
		textFieldName.setHorizontalAlignment(SwingConstants.CENTER);
		panel1.add(textFieldName);
		
		JLabel lblPrice = new JLabel("Prix HT");
		contentPane.add(lblPrice);
		
		JPanel panel2 = new JPanel();
		contentPane.add(panel2);
		spinnerPrice = new JSpinner(new SpinnerNumberModel(product.getPrice(), 0.0, 9999999999.0, 0.01));
		panel2.add(spinnerPrice);
		
		JLabel lblTVA = new JLabel("TVA");
		contentPane.add(lblTVA);
		
		JPanel panel3 = new JPanel();
		contentPane.add(panel3);
		spinnerTVA = new JSpinner(new SpinnerNumberModel(product.getTVA(), 0.0, 100, 0.01));
		panel3.add(spinnerTVA);
		
		JLabel lblQuantity = new JLabel("Quantité");
		contentPane.add(lblQuantity);
		
		JPanel panel4 = new JPanel();
		contentPane.add(panel4);
		spinnerQuantity = new JSpinner(new SpinnerNumberModel(product.getQuantity(), 0, Integer.MAX_VALUE, 1));
		panel4.add(spinnerQuantity);
		
		JLabel lblShelf = new JLabel("Rayon");
		contentPane.add(lblShelf);
		
		JPanel panel5 = new JPanel();
		contentPane.add(panel5);
		comboShelf = new JComboBox<>(database.getShelves().toArray()); 
		panel5.add(comboShelf);
		
		JPanel panel = new JPanel();
		contentPane.add(panel);
		
		lblError = new JLabel("");
		panel.add(lblError);
		
		JPanel panel6 = new JPanel();
		contentPane.add(panel6);
		panel6.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 5));
		
		JButton btnBack = new JButton("Valider");
		btnBack.addActionListener(this);
		panel6.add(btnBack);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// first name
		String name = textFieldName.getText();
		if (name.equals("")) {
			lblError.setText("Veuillez-remplir le champ Nom");
			return;
		}
		
		try {
			product.setName(name);
		} catch(IllegalArgumentException exc) {
			lblError.setText(exc.getMessage());
			return;
		}
		
		product.setPrice((Double) spinnerPrice.getValue());
		product.setQuantity((int) spinnerQuantity.getValue());
		product.setTVA((Double) spinnerTVA.getValue());
		product.setShelf((Shelf) comboShelf.getSelectedItem());
		
		view.updateView();
		dispose();
	}

}
