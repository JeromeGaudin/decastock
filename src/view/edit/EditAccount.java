package view.edit;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import controller.EmployeeController;
import model.Database;
import model.EmployeeAccount;
import model.Shelf;
import view.AppWindow;

/**
 * Window to edit an account
 */
public class EditAccount extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	
    /**
     * choice allows to answer.
     */
    private static final String[] choice = {"non" ,"oui"};
	
    /**
     * to set the last name of the account
     */
	private JTextField textFieldName;
    /**
     * to set the first name of the account
     */
	private JTextField textFieldFirstName;
    /**
     * to set the login of the account
     */
	private JTextField textFieldId;
    /**
     * to set the password name of the account
     */
	private JTextField textFieldPassword;
    /**
     * to set if the account can have the right to login
     */
	private JComboBox<?> comboAccessRight;
    /**
     * to set of witch shelf this account is responsible
     */
	private JComboBox comboShelf;
    /**
     * to have access to all data on database
     */
	private Database database;
	/**
	 * JLabel to print error
	 */
	private JLabel lblError;
	/**
	 * EmployeeAccount to update
	 */
	private EmployeeAccount employee;

	/**
	 * Constructor
	 * @param dataBase where all data are stored
	 * @param employeeToUpdate
	 */
	public EditAccount(Database dataBase, EmployeeAccount employeeToUpdate) {
		employee = employeeToUpdate;
		this.database = dataBase;
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 700, 400);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(8,2));
		
		JLabel lblName = new JLabel("Nom");
		contentPane.add(lblName);
		
		JPanel panel1 = new JPanel();
		contentPane.add(panel1);
		
		textFieldName = new JTextField(employee.getLastName(), 10);
		panel1.add(textFieldName);
		
		JLabel lblFirstName = new JLabel("Prénom");
		contentPane.add(lblFirstName);
		
		JPanel panel2 = new JPanel();
		contentPane.add(panel2);
		
		textFieldFirstName = new JTextField(employee.getFirstName(), 10);
		panel2.add(textFieldFirstName);
		
		JLabel lblId = new JLabel("Identifiant");
		contentPane.add(lblId);
		
		JPanel panel3 = new JPanel();
		contentPane.add(panel3);
		
		textFieldId = new JTextField(employee.getLogin(), 10);
		textFieldId.setEditable(false);
		panel3.add(textFieldId);
		
		JLabel lblPassword = new JLabel("Mot de passe");
		contentPane.add(lblPassword);
		
		JPanel panel4 = new JPanel();
		contentPane.add(panel4);
		
		textFieldPassword = new JTextField(employee.getPassword(), 10);
		panel4.add(textFieldPassword);
		
		JLabel lblAccessRight = new JLabel("Droit d'accès actifs");
		contentPane.add(lblAccessRight);
		
		JPanel panel5 = new JPanel();
		contentPane.add(panel5);
		
		comboAccessRight = new JComboBox<>(choice);
		int index = employee.isAccess()? 1: 0;
		comboAccessRight.setSelectedIndex(index);
		panel5.add(comboAccessRight);
		
		JLabel lblShelf = new JLabel("Son rayon");
		contentPane.add(lblShelf);
		
		JPanel panel6 = new JPanel();
		contentPane.add(panel6);
		
	   	DefaultComboBoxModel comboBoxModelShelf = new DefaultComboBoxModel();
	   	comboBoxModelShelf.addElement("Aucun");
    	for (Shelf s : dataBase.getShelves()) {
    		comboBoxModelShelf.addElement(s);
    	}
		comboShelf = new JComboBox(comboBoxModelShelf);
		if (employee.getShelf() == null) {
			comboShelf.setSelectedIndex(0);
		} else {
			comboShelf.setSelectedItem(employee.getShelf());
		}
		panel6.add(comboShelf);
		
		JPanel panel7 = new JPanel();
		contentPane.add(panel7);
		
		lblError = new JLabel("");
		lblError.setForeground(Color.RED);
		panel7.add(lblError);
		
		JPanel panel8 = new JPanel();
		contentPane.add(panel8);
		panel8.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 5));
		
		JPanel panelEmpy1 = new JPanel();
		contentPane.add(panelEmpy1);
		
		JPanel panel9 = new JPanel();
		contentPane.add(panel9);
		
		JButton btnBack = new JButton("Valider");
		panel9.add(btnBack);
		btnBack.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// first name
		String firstName = textFieldFirstName.getText();
		if (firstName.equals("")) {
			lblError.setText("Veuillez-remplir le champ Prénom");
			return;
		}
		// last name
		String lastName = textFieldName.getText();
		if (lastName.equals("")) {
			lblError.setText("Veuillez-remplir le champ Nom");
			return;
		}
		// password
		String password = textFieldPassword.getText();
		if (password.equals("")) {
			lblError.setText("Veuillez-remplir le champ Mot de passe");
			return;
		}
		// shelf
		Shelf shelf = null;
		if (comboShelf.getSelectedItem().getClass().equals(Shelf.class)) {
			shelf = (Shelf) comboShelf.getSelectedItem();
		}
		
		try {
			employee.setFirstName(firstName);
			employee.setLastName(lastName);
			employee.setPassword(password);
			
			EmployeeController employeeController = new EmployeeController();
			employeeController.moveEmployee(employee, shelf);
		} catch(IllegalArgumentException exception) {
			lblError.setText(exception.getMessage());
			return;
		}
		// access
		if (comboAccessRight.getSelectedIndex() == 1) {
			employee.setAccess(true);
		}

		
		AppWindow appWindow = AppWindow.getAppWindow();
		appWindow.getSeeAllAccount().updateView(); // update the view
		dispose(); // close window
	}

}
