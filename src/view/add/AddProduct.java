package view.add;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;

import controller.ProductController;
import model.Database;
import model.Product;
import model.Shelf;
import view.AppWindow;

/**
 * window to add a product
 */
public class AddProduct extends JFrame implements ActionListener {
	
	/**
	 * JTextField to set the name of product
	 */
	private JTextField textFieldName;
	
	/**
	 * JSpinner to set the price of product
	 */
	private JSpinner spinnerPrice;
	/**
	 * JSpinner to set the quantity of product
	 */
	private JSpinner spinnerQuantity;
	/**
	 * JSpinner to set the TVA of product
	 */
	private JSpinner spinnerTVA;
	
	/**
	 * JComboBox to set the shelf where are the product
	 */
	private JComboBox comboShelf;
	
	/**
	 * JLabel to display error
	 */
	private JLabel lblError;
	
	/**
	 * 
	 */
	private Database database;
	

	/**
	 * Create the frame.
	 * @param database where all data are stored
	 */
	public AddProduct(Database database) {
		this.database = database;
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1100, 500);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(6,2));
		
		JLabel lblName = new JLabel("Nom");
		contentPane.add(lblName);
		
		JPanel panel1 = new JPanel();
		contentPane.add(panel1);
		
		textFieldName = new JTextField();
		panel1.add(textFieldName);
		textFieldName.setColumns(10);
		
		
		JLabel lblPrice = new JLabel("Prix HT");
		contentPane.add(lblPrice);
		
		JPanel panel2 = new JPanel();
		contentPane.add(panel2);
		
		spinnerPrice = new JSpinner(new SpinnerNumberModel(0.01, 0.01, 9999999999.0, 0.01));
		panel2.add(spinnerPrice);
		
		
		JLabel lblTVA = new JLabel("TVA");
		contentPane.add(lblTVA);
		
		JPanel panel3 = new JPanel();
		contentPane.add(panel3);
		
		spinnerTVA = new JSpinner(new SpinnerNumberModel(0.01, 0.01, 100, 0.01));
		panel3.add(spinnerTVA);
		
		
		JLabel lblQuantity = new JLabel("Quantité");
		contentPane.add(lblQuantity);
		
		JPanel panel4 = new JPanel();
		contentPane.add(panel4);
		
		spinnerQuantity = new JSpinner(new SpinnerNumberModel(1, 0, Integer.MAX_VALUE, 1));
		panel4.add(spinnerQuantity);
		
		
		JLabel lblShelf = new JLabel("Rayon");
		contentPane.add(lblShelf);
		
		JPanel panel5 = new JPanel();
		contentPane.add(panel5);
		
		comboShelf = new JComboBox(database.getShelves().toArray()); 
		panel5.add(comboShelf);
		
		
		JPanel panel6 = new JPanel();
		contentPane.add(panel6);
		
		lblError = new JLabel("");
		lblError.setForeground(Color.RED);
		panel6.add(lblError);
		
		JPanel panel7 = new JPanel();
		contentPane.add(panel7);
		panel7.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JButton btnSubmit = new JButton("Valider");
		btnSubmit.addActionListener(this);
		panel7.add(btnSubmit);
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		// first name
		String name = textFieldName.getText();
		if (name.equals("")) {
			lblError.setText("Veuillez-remplir le champ Nom");
			return;
		}
		
		
		Shelf shelf = (Shelf) comboShelf.getSelectedItem();

		double price = (double) spinnerPrice.getValue();
		double tVA = (double) spinnerTVA.getValue();
		int quantity = (int) spinnerQuantity.getValue();
		
		Product product = null;
		try {
			product = new Product(name, price, tVA, quantity, shelf);
		} catch (IllegalArgumentException exc) {
			lblError.setText(exc.getMessage());
			return;
		}
		
		ProductController productController = new ProductController();
		productController.addProduct(product, database);
		
		AppWindow appWindow = AppWindow.getAppWindow();
		appWindow.getSeeShelf().updateView(); // update the view
		dispose(); // close window
	}

}
