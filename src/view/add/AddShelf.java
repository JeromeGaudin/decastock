package view.add;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.EmployeeController;
import controller.ShelfController;
import model.Database;
import model.EmployeeAccount;
import model.Shelf;
import view.AppWindow;

/**
 * window to add a shelf
 */
public class AddShelf extends JFrame implements ActionListener {
	
	/**
	 * JTextField to get the name of thee shelf
	 */
    private JTextField textFieldName;

    /**
     * JComboBox to get the manager of this shelf
     */
    private JComboBox comboDepartmentManager;
    
    /**
     * to print error of insertion
     */
    private JLabel lblErrorName;
    
    /**
     * database where all data are stored
     */
    private Database database;
    
    
    /**
     * Create the panel.
     * @parma database where all data are stored
     */
    public AddShelf(Database database) {
    	this.database = database;
    	
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1100, 500);
        getContentPane().setLayout(new GridLayout(4, 2, 15, 15));
        
        JLabel lblName = new JLabel("Nom");
        getContentPane().add(lblName);
        
        JPanel panel1 = new JPanel();
        getContentPane().add(panel1);
        panel1.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
        
        textFieldName = new JTextField();
        panel1.add(textFieldName);
        textFieldName.setColumns(10);
        
        
        JLabel lblDepatmentManager = new JLabel("Chef de rayon");
        getContentPane().add(lblDepatmentManager);
        
        JPanel panel2 = new JPanel();
        getContentPane().add(panel2);
        
        DefaultComboBoxModel comboBoxModelEmployee = new DefaultComboBoxModel();
        EmployeeController employeeController = new EmployeeController();
        comboBoxModelEmployee.addElement("Aucun");
        for (EmployeeAccount emp : employeeController.getEmployeeManageNothing(database)) {
        	comboBoxModelEmployee.addElement(emp);
    	}
        comboDepartmentManager = new JComboBox(comboBoxModelEmployee);
        panel2.add(comboDepartmentManager);
        
        lblErrorName = new JLabel(" ");
        lblErrorName.setForeground(Color.RED);
        getContentPane().add(lblErrorName);
        
        JPanel panelEmpty1 = new JPanel();
        getContentPane().add(panelEmpty1);
        
        JPanel panelEmpty2 = new JPanel();
        getContentPane().add(panelEmpty2);
        
        JPanel panel = new JPanel();
        getContentPane().add(panel);
        
        JButton btnValidate = new JButton("Valider");
        panel.add(btnValidate);
        btnValidate.addActionListener(this);
    }


	@Override
	public void actionPerformed(ActionEvent arg0) {
		// name
		String name = textFieldName.getText();
		if (name.equals("")) {
			lblErrorName.setText("Veuillez-remplir le champ Nom");
			return;
		}
		
		Shelf shelf = null;
		try {
			 shelf = new Shelf(name);
			 
			 ShelfController shelfController = new ShelfController();
			 shelfController.addShelf(shelf, database);
		} catch (IllegalArgumentException exc) {
			lblErrorName.setText(exc.getMessage());
			return;
		}
		
		AppWindow appWindow = AppWindow.getAppWindow();
		appWindow.getSAllShelf().updateView(); // update the view
		dispose(); // close window
	}

}
