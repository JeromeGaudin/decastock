package view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import controller.AdminController;
import controller.DatabaseManagement;
import controller.EmployeeController;
import model.AdminAccount;
import model.Database;
import model.EmployeeAccount;
import model.Setting;

/**
 * create a window to the connection
 */
public class Connection extends JFrame implements ActionListener{
	private static final long serialVersionUID = 1L;
	
	private JTextField textFieldLogin;
	private JTextField textFieldPassword;
	
	private JLabel lblError;
	
	private Database database;

	/**
	 * Create the frame.
	 */
	public Connection() {
		// restore database
		DatabaseManagement databaseManagement = new DatabaseManagement();
		database = databaseManagement.loadDatabase(Setting.BACKUP_FILE_PATH);
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JLabel lblLogin = new JLabel("Identifiant");
		GridBagConstraints gbcLabelLogin = new GridBagConstraints();
		gbcLabelLogin.anchor = GridBagConstraints.EAST;
		gbcLabelLogin.insets = new Insets(0, 0, 5, 5);
		gbcLabelLogin.gridx = 4;
		gbcLabelLogin.gridy = 3;
		add(lblLogin, gbcLabelLogin);
		
		textFieldLogin = new JTextField();
		GridBagConstraints gbcTextFieldLogin = new GridBagConstraints();
		gbcTextFieldLogin.anchor = GridBagConstraints.WEST;
		gbcTextFieldLogin.insets = new Insets(0, 0, 5, 0);
		gbcTextFieldLogin.gridx = 6;
		gbcTextFieldLogin.gridy = 3;
		add(textFieldLogin, gbcTextFieldLogin);
		textFieldLogin.setColumns(10);
		
		JLabel lblPassword = new JLabel("Mot de passe");
		GridBagConstraints gbcLblPassword = new GridBagConstraints();
		gbcLblPassword.insets = new Insets(0, 0, 5, 5);
		gbcLblPassword.anchor = GridBagConstraints.EAST;
		gbcLblPassword.gridx = 4;
		gbcLblPassword.gridy = 5;
		add(lblPassword, gbcLblPassword);
		
		textFieldPassword = new JTextField();
		GridBagConstraints gbcTextFieldPassword = new GridBagConstraints();
		gbcTextFieldPassword.insets = new Insets(0, 0, 5, 0);
		gbcTextFieldPassword.anchor = GridBagConstraints.WEST;
		gbcTextFieldPassword.gridx = 6;
		gbcTextFieldPassword.gridy = 5;
		add(textFieldPassword, gbcTextFieldPassword);
		textFieldPassword.setColumns(10);
		
		lblError = new JLabel("");
		GridBagConstraints gbcLblError = new GridBagConstraints();
		gbcLblError.insets = new Insets(0, 0, 5, 5);
		gbcLblError.gridx = 4;
		gbcLblError.gridy = 6;
		add(lblError, gbcLblError);
		
		JButton btnValidate = new JButton("Valider");
		btnValidate.addActionListener(this);
		GridBagConstraints gbcBtnValidate = new GridBagConstraints();
		gbcBtnValidate.anchor = GridBagConstraints.WEST;
		gbcBtnValidate.gridx = 6;
		gbcBtnValidate.gridy = 7;
		add(btnValidate, gbcBtnValidate);
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		String login = textFieldLogin.getText();
		String password = textFieldPassword.getText();
		
		AdminController adminController = new AdminController();
		AdminAccount admin = adminController.connection(login, password, database);
		EmployeeController employeeController = new EmployeeController();
		EmployeeAccount employee = employeeController.connection(login, password, database);

		if (admin != null) {
			if ( !admin.isAccess()) {
				lblError.setText("Compte révoqué");
			} else {
				AppWindow appWindow = AppWindow.initialization(database);
				dispose();
				appWindow.setVisible(true);
			}
		} else if (employee != null) {
			if ( !employee.isAccess()) {
				lblError.setText("Compte révoqué");
			} else {
				AppWindow appWindow = AppWindow.initialization(employee, database);
				dispose();
				appWindow.setVisible(true);
			}
		} else {
			lblError.setText("Mauvais mot de passe ou mauvais Identifiant");
		}
	}

}
