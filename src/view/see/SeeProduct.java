package view.see;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.Product;

import java.awt.FlowLayout;

public class SeeProduct extends JFrame implements ActionListener {
	

	/**
	 * Create the frame.
	 */
	public SeeProduct(Product product) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(6,2));
		
		JLabel lblName = new JLabel("Nom");
		contentPane.add(lblName);
		
		JLabel labelName = new JLabel(product.getName());
		contentPane.add(labelName);
		
		JLabel lblPrice = new JLabel("Prix HT");
		contentPane.add(lblPrice);
		
		JLabel labelPrice = new JLabel(Double.toString(product.getPrice()));
		contentPane.add(labelPrice);
		
		JLabel lblTVA = new JLabel("TVA");
		contentPane.add(lblTVA);
		
		JLabel labelTVA = new JLabel(Double.toString(product.getTVA()));
		contentPane.add(labelTVA);
		
		JLabel lblQuantity = new JLabel("Quantité");
		contentPane.add(lblQuantity);
		
		JLabel labelQuantity = new JLabel(Integer.toString(product.getQuantity()));
		contentPane.add(labelQuantity);
		
		JLabel lblShelf = new JLabel("Rayon");
		contentPane.add(lblShelf);
		
		JLabel labelShelf = new JLabel(product.getShelf().getName());
		contentPane.add(labelShelf);
		
		JPanel panel = new JPanel();
		contentPane.add(panel);
		
		JPanel panel2 = new JPanel();
		contentPane.add(panel2);
		panel2.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 5));
		
		JButton btnBack = new JButton("Retour");
		btnBack.addActionListener(this);
		panel2.add(btnBack);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		dispose();
	}

}
