package view.see;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import controller.EmployeeController;
import model.Account;
import model.Database;
import model.EmployeeAccount;
import view.View;
import view.add.AddAccount;
import view.component.EmployeeAccountTableModel;
import view.component.ButtonColumn;
import view.component.SelectShelfTableModel;
import view.edit.EditAccount;

public class SeeAllAccount extends JPanel implements View, ActionListener {
	private static final long serialVersionUID = 1L;
	
	/**
	 * table to see all account
	 */
	private JTable table;
	
	/**
	 * database to get all data of each account
	 */
	private Database database;
	
	/**
	 * button for add a new account
	 */
	private JButton btnNewAccount;

	/**
	 * Create the panel.
	 */
	public SeeAllAccount(Database dataBase) {
		database = dataBase;
		
		setLayout(new BorderLayout(0, 0));
		
		EmployeeAccountTableModel atm = new EmployeeAccountTableModel();
		table = new JTable(atm);
		
		add(new JScrollPane(table), BorderLayout.CENTER);
	
		
		// add column of button with action view
		Action edit = new AbstractAction() {
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent e) {
		        JTable jTable = (JTable)e.getSource();
		        int modelRow = Integer.parseInt(e.getActionCommand());
		        EmployeeAccountTableModel model = (EmployeeAccountTableModel)jTable.getModel();
		        EmployeeAccount employee = model.getData(modelRow);
		        
		        EditAccount editAccount = new EditAccount(database, employee);
		        editAccount.setVisible(true);
			}
		};
		new ButtonColumn(table, edit, 1);
		
		
		// add column of button with action edit
		Action delete = new AbstractAction() {
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent e) {
		        JTable jTable = (JTable)e.getSource();
		        int modelRow = Integer.parseInt(e.getActionCommand());
		        EmployeeAccountTableModel model = (EmployeeAccountTableModel)jTable.getModel();
		        EmployeeAccount employee = model.getData(modelRow);
		        
		        EmployeeController employeeController = new EmployeeController();
		        employeeController.removeEmployee(employee, database);
		        
		        updateView();
			}
		};
		new ButtonColumn(table, delete, 2);
		
		
		JPanel panel = new JPanel();
		add(panel, BorderLayout.SOUTH);
		
		btnNewAccount = new JButton("Nouveau compte");
		btnNewAccount.addActionListener(this);
		panel.add(btnNewAccount);
	}

	@Override
	public void updateView() {
		List<EmployeeAccount> accounts = database.getEmployees();
		
		EmployeeAccountTableModel atm = (EmployeeAccountTableModel) table.getModel();
		atm.setData(accounts);
		atm.fireTableDataChanged();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnNewAccount) {
			AddAccount addAccount = new AddAccount(database);
			addAccount.setVisible(true);
		}
	}

}
