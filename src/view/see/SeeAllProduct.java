package view.see;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import model.Database;
import model.Product;
import model.Shelf;
import view.AppWindow;
import view.View;
import view.add.AddProduct;
import view.component.ButtonColumn;
import view.component.ProductEditTableModel;
import view.component.SomeProductEditableTableModel;
import view.edit.EditProduct;

public class SeeAllProduct extends JPanel implements View, ActionListener {
	private static final long serialVersionUID = 1L;

	/**
	 * JTable table to see all product in a shelf
	 */
	private JTable table;
	
	/**
	 * reference of database to get information of each product in this shelf
	 */
	private Database database;
	
	/**
	 * The object Shelf which contains all product to see 
	 */
	private Shelf shelf;

	/**
	 * JButton to add a new Product in a shelf
	 */
	private JButton btnNewProduct;
	
	/**
	 * Create the panel.
	 */
	public SeeAllProduct(Database dataBase, Shelf shelfEditable) {
		database = dataBase;
		shelf = shelfEditable;
		
		setLayout(new BorderLayout(0, 0));
		
		SomeProductEditableTableModel tableModel = new SomeProductEditableTableModel(shelfEditable);
		table = new JTable(tableModel);
		
		add(new JScrollPane(table), BorderLayout.CENTER);
		
		// add column of button with action decrement quantity
		Action decrementQuantity = new AbstractAction() {
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent e) {
		        JTable jTable = (JTable)e.getSource();
		        int modelRow = Integer.parseInt(e.getActionCommand());
		        
		        ProductEditTableModel model = (ProductEditTableModel)jTable.getModel();
		        int value = (int) model.getValueAt(modelRow, 2);
		        try {
		        	model.setValueAt(value - 1, modelRow, 2);
		        	model.fireTableCellUpdated(modelRow, 2);
		        } catch(IllegalArgumentException exc) {
		        	// do nothing
		        }
			}
		};
		new ButtonColumn(table, decrementQuantity, 1);
		
		
		// add column of button with action increment quantity
		Action incrementQuantity = new AbstractAction() {
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent e) {
		        JTable jTable = (JTable)e.getSource();
		        int modelRow = Integer.parseInt(e.getActionCommand());
		        
		        ProductEditTableModel model = (ProductEditTableModel)jTable.getModel();
		        int value = (int) model.getValueAt(modelRow, 2);
		        try {
		        	model.setValueAt(value + 1, modelRow, 2); 
		        	model.fireTableCellUpdated(modelRow, 2);
		        } catch(IllegalArgumentException exc) {
		        	// do nothing
		        }
			}
		};
		new ButtonColumn(table, incrementQuantity, 3);
		
		// add column of button with action view
		Action view = new AbstractAction() {
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent e) {
		        JTable jTable = (JTable)e.getSource();
		        int modelRow = Integer.parseInt(e.getActionCommand());
		        ProductEditTableModel model = (ProductEditTableModel)jTable.getModel();
		        Product product = model.getData(modelRow);
		        
		        SeeProduct seeProduct = new SeeProduct(product);
		        seeProduct.setVisible(true);
			}
		};
		new ButtonColumn(table, view, 4);
		
		
		// add column of button with action edit
		Action edit = new AbstractAction() {
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent e) {
		        JTable jTable = (JTable)e.getSource();
		        int modelRow = Integer.parseInt(e.getActionCommand());
		        ProductEditTableModel model = (ProductEditTableModel)jTable.getModel();
		        Product product = model.getData(modelRow);
		        
		        AppWindow appWindow = AppWindow.getAppWindow();
		        View view = appWindow.getSeeAllProduct();
		        
		        EditProduct editProduct = new EditProduct(view, product, dataBase);
		        editProduct.setVisible(true);
			}
		};
		new ButtonColumn(table, edit, 5);
		
		
		JPanel panel = new JPanel();
		add(panel, BorderLayout.SOUTH);
		
		btnNewProduct = new JButton("Nouveau produit");
		btnNewProduct.addActionListener(this);
		panel.add(btnNewProduct);
	}

	@Override
	public void updateView() {
		if (shelf != null) {
			List<Product> products = shelf.getProduts();
		
			SomeProductEditableTableModel tableModel = (SomeProductEditableTableModel) table.getModel();
			tableModel.setData(products);
			tableModel.fireTableDataChanged();
		}
		
		if (database.getShelves().isEmpty()) {
			btnNewProduct.setEnabled(false);
		} else {
			btnNewProduct.setEnabled(true);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnNewProduct) {
			AddProduct addProduct = new AddProduct(database);
			addProduct.setVisible(true);
		}
	}
	
	// setters
	public void setShelf(Shelf newShelf) {
		shelf = newShelf;
	}

}
