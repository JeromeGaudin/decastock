package view.see;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import model.Database;
import model.Shelf;
import view.AppWindow;
import view.View;
import view.add.AddShelf;
import view.component.ButtonColumn;
import view.component.SelectShelfTableModel;

public class SeeAllShelf extends JPanel implements View, ActionListener {
	private JTable table;
	
	private JButton btnNewShelf;
	
	private Database database;

	/**
	 * Create the panel.
	 */
	public SeeAllShelf(Database database) {
		this.database = database;
		
		setLayout(new BorderLayout(0, 0));
		
		SelectShelfTableModel sstm = new SelectShelfTableModel();
		table = new JTable(sstm);
		
		// add column of button with action delete
		Action delete = new AbstractAction() {
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent e) {
		        JTable jTable = (JTable)e.getSource();
		        int modelRow = Integer.parseInt( e.getActionCommand() );
		        Shelf shelf = ((SelectShelfTableModel)jTable.getModel()).getData(modelRow);
		        
		        AppWindow appWindow = AppWindow.getAppWindow();
		        SeeShelf seeShelf = appWindow.getSeeShelf();
		        seeShelf.setShelf(shelf);
		        appWindow.getTabbedPane().setSelectedComponent(seeShelf);
			}
		};
		add(new JScrollPane(table), BorderLayout.CENTER);
		
		ButtonColumn btnColumnDelet = new ButtonColumn(table, delete, 1);
		
		
		JPanel panel = new JPanel();
		add(panel, BorderLayout.SOUTH);
		
		btnNewShelf = new JButton("Nouveau Rayon");
		btnNewShelf.addActionListener(this);
		panel.add(btnNewShelf);
	}

	@Override
	public void updateView() {
		List<Shelf> shelves = database.getShelves();
		
		SelectShelfTableModel sstm = (SelectShelfTableModel) table.getModel();
		sstm.setData(shelves);
		sstm.fireTableDataChanged();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnNewShelf) {
			AddShelf addShelf = new AddShelf(database);
			addShelf.setVisible(true);
		}
	}

}
