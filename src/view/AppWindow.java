package view;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.WindowConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import controller.DatabaseManagement;
import model.AdminAccount;
import model.Database;
import model.EmployeeAccount;
import model.Setting;
import model.Shelf;
import view.see.SeeAllAccount;
import view.see.SeeAllProduct;
import view.see.SeeAllShelf;
import view.see.SeeShelf;

public class AppWindow extends JFrame implements ChangeListener, WindowListener {
	private static final long serialVersionUID = 1L;
	
	/**
	 * appWindow it's to use the design pattern of singleton 
	 */
	private static AppWindow appWindow = null;
	
	/**
	 * tabbedPane contains all tabs.
	 */
	private JTabbedPane tabbedPane;
	
	/**
	 * Database where all data are stored
	 */
	private Database database;
	
	/**
	 * tabbedPane to see all product in a shelf
	 */
	private SeeShelf seeShelf;
	
	/**
	 * tabbedPane to see all shelf
	 */
	private SeeAllShelf seeAllShelf;
	
	/**
	 * tabbedPane to see all shelves
	 */
	private SeeAllAccount seeAllAccount;
	
	/**
	 * to see all product in the shop
	 */
	private SeeAllProduct seeAllProduct;
	
	/**
	 * method to get an instance of AppWindow
	 * Before to call this method it need to call initialization method
	 * @return an instance of Appwindow
	 */
	public static AppWindow getAppWindow() {
		return appWindow;
	}
	
	/**
	 * initialization is the method for initialize AppWindow
	 * This method must be called before other
	 * @param account EmployeeAccount of employee who open application
	 * @param database database where are store all data
	 * @return an object AppWindow
	 */
	public static AppWindow initialization(EmployeeAccount account, Database database) {
		appWindow = new AppWindow(account, database);
		return appWindow;
	}
	
	/**
	 * initialization is the method for initialize AppWindow
	 * This method must be called before other
	 * This method is called if it's an administrator who open application
	 * @param database database where are store all data
	 * @return an object AppWindow
	 */
	public static AppWindow initialization(Database database) {
		appWindow = new AppWindow(database);
		return appWindow;
	}
	
	/**
	 * Constructor in private, we need to call initialization and next getAppwindow to have an object AppWindow
	 * @param account of employee who open application
	 * @param database where all data are stored
	 */
	private AppWindow(EmployeeAccount account, Database database) {	
		this.database = database;
		
		// window setting
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setMinimumSize(new Dimension(500, 300));
		setSize(1000, 600);
		setTitle("DecaStock");
		addWindowListener(this);
		
		//layout
		setLayout(new GridLayout(1, 1));
		
		tabbedPane = new JTabbedPane();
		tabbedPane.addChangeListener(this);
		
		/// For a user
		Shelf hisShelf = account.getShelf();
		
		// his shelf
		seeShelf = new SeeShelf(database, hisShelf);
		tabbedPane.add("Mon rayon", seeShelf);
		
		// all product
		seeAllProduct = new SeeAllProduct(database, hisShelf);
		tabbedPane.add("Magasin", seeAllProduct);
		
		
		add(tabbedPane);
	}
	
	/**
	 * Constructor in private, we need to call initialization and next getAppwindow to have an object AppWindow
	 * constructor used when it's an administrator who is connect
	 * @param database where all data are stored
	 */
	private AppWindow(Database database) {	
		this.database = database;
		
		// window setting
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setMinimumSize(new Dimension(500, 300));
		setSize(1000, 600);
		setTitle("DecaStock");
		addWindowListener(this);
		
		//layout
		setLayout(new GridLayout(1, 1));
		
		tabbedPane = new JTabbedPane();
		tabbedPane.addChangeListener(this);
		
		/// For an admin
		
		// All shelf
		seeAllShelf = new SeeAllShelf(database);
		tabbedPane.add("Rayons", seeAllShelf);
		
		// one shelf
		seeShelf = new SeeShelf(database, null);
		tabbedPane.add("Un rayon", seeShelf);
		
		// All account
		seeAllAccount = new SeeAllAccount(database);
		tabbedPane.add("Gestion des comptes", seeAllAccount);
		
		add(tabbedPane);
	}

	/**
	 * This method allows to update chosen tab. It is called each time we change tab. 
	 * @param event
	 */
	@Override
	public void stateChanged(ChangeEvent event) {
		View view = (View) tabbedPane.getSelectedComponent();
		view.updateView();
	}

	@Override
	public void windowActivated(WindowEvent arg0) {
		//noting
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
		//noting
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		// when the window close save and wirte all data on database in a file 
		DatabaseManagement databaseManagement = new DatabaseManagement();
		databaseManagement.saveDatabase(Setting.BACKUP_FILE_PATH, database);
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		//noting
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		//noting
	}

	@Override
	public void windowIconified(WindowEvent arg0) {	
		//noting
	}
	@Override
	public void windowOpened(WindowEvent arg0) {
		//noting
	}
	
	// setters and getters
	public SeeShelf getSeeShelf() {
		return seeShelf;
	}
	
	public SeeAllAccount getSeeAllAccount() {
		return seeAllAccount;
	}
	
	public SeeAllShelf getSAllShelf() {
		return seeAllShelf;
	}
	
	
	public JTabbedPane getTabbedPane() {
		return tabbedPane;
	}

	public View getSeeAllProduct() {
		return seeAllProduct;
	}
	
	
}