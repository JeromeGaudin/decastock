package controller;

import model.Database;
import model.Shelf;

/**
 * class to manage a shelf
 */
public class ShelfController {
	
	/**
	 * method to add a shelf to a database
	 * @param newShelf to add
	 * @param dataBase where shelf will be added
	 */
	public void addShelf(Shelf newShelf, Database dataBase) {
		dataBase.addShelf(newShelf);
	}
}
