package controller;

import java.util.List;

import model.AdminAccount;
import model.Database;
/**
 * Controller used by an administrator
 */
public class AdminController {
	
	/**
	 * connect if login and password match among those in database
	 * @param login of admin
	 * @param password of admin
	 * @param database where are store account
	 * @return an object AdminAccount or null if login or password don't match
	 */
	public AdminAccount connection(String login, String password, Database database) {
		List<AdminAccount> admins = database.getAdmins();
		for (AdminAccount admin : admins) {
			if (admin.getLogin().equals(login) && admin.getPassword().contentEquals(password)) {
				return admin;
			}
		}
		return null;
	}
}
