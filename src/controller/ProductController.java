package controller;

import model.Database;
import model.Product;
import model.Shelf;

/**
 * Class to manage a product
 */
public class ProductController {
	
	/**
	 * to add a product on database
	 * @param product to add
	 * @param database where the product will be added
	 */
	public void addProduct(Product product, Database database) {
		Shelf shelf = product.getShelf();
		shelf.addProduct(product);
		
		database.addProduct(product);
	}
	
	/**
	 * to move a product between of shelf
	 * you must not change the shelf directly in the product object
	 * @param product to move
	 * @param newShelf where the product will be
	 */
	public void moveProduct(Product product, Shelf newShelf) {
		//remove product in the old shelf
		Shelf oldShelf = product.getShelf();
		oldShelf.removeProduct(product);
		
		//edit product's attribute
		product.setShelf(newShelf);
		
		//add product in the new shelf
		newShelf.addProduct(product);
	}
}
