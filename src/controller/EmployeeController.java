package controller;

import java.util.ArrayList;
import java.util.List;

import model.Database;
import model.EmployeeAccount;
import model.Shelf;

/**
 * Class to manage an employee
 */
public class EmployeeController {
	
	/**
	 * to add an employee in a database
	 * @param employee to add
	 * @param dataBase where employee will be added
	 * @return true if employee is added
	 */
	public boolean addEmployee(EmployeeAccount employee, Database dataBase) {
		String newLogin = employee.getLogin();
		List<EmployeeAccount> listEmp = dataBase.getEmployees();
		
		for (EmployeeAccount emp : listEmp) {
			if (emp.getLogin().equals(newLogin)) {
				//employee is not added because its login is not unique
				return false;
			}
		}
		//add employee in its shelf
		Shelf shelf = employee.getShelf();
		if (shelf != null) {
			shelf.addEmployee(employee);
		}
		//add employee in the database
		dataBase.addEmployee(employee);
		return true;
	}
	
	/**
	 * To change an employee of shelf
	 * you must not add new shelf in employee
	 * @param employee to edit
	 * @param newShelf to attributed to the employee
	 */
	public void moveEmployee(EmployeeAccount employee, Shelf newShelf) {
		//remove employee in the old shelf
		Shelf oldShelf = employee.getShelf();
		if (oldShelf != null) {
			oldShelf.removeEmployee(employee);
		}
		//edit employee 's attribute
		employee.setShelf(newShelf);
		//add employee in the new shelf
		if (newShelf != null) {
			newShelf.addEmployee(employee);
		}
	}
	
	/**
	 * remove an employee on a database
	 * @param employee to remove
	 * @param dataBase where employee will be removed
	 */
	public void removeEmployee(EmployeeAccount employee, Database dataBase) {
		//remove employee in the shelf
		Shelf oldShelf = employee.getShelf();
		if (oldShelf != null) {
			oldShelf.removeEmployee(employee);
		}
		//remove employee in the database
		dataBase.removeEmployee(employee);
	}
	
	/**
	 * To get a list of all employee who have not shelf to manage
	 * @param database in which see
	 * @return the list of all employee who have not shelf to manage
	 */
	public List<EmployeeAccount> getEmployeeManageNothing(Database database) {
		List<EmployeeAccount> listRes = new ArrayList<>();
		List<EmployeeAccount> listEmp = database.getEmployees();
		
		for (EmployeeAccount emp : listEmp) {
			if (emp.getShelf() == null) {
				listRes.add(emp);
			}
		}
		return listRes;
	}
	
	/**
	 * To connect an employee
	 * @param login of employee
	 * @param password of employee
	 * @param database where should be
	 * @return EmployeeAccount or null if login or password don't match among employees in database
	 */
	public EmployeeAccount connection(String login, String password, Database database) {
		List<EmployeeAccount> employees = database.getEmployees();
		for (EmployeeAccount emp : employees) {
			if (emp.getLogin().equals(login) && emp.getPassword().contentEquals(password)) {
				return emp;
			}
		}
		return null;
	}
}
