package controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import model.Database;

/**
 * Class to manage the database
 */
public class DatabaseManagement {
	/**
	 * To print error message 
	 */
	private static final Logger LOGGER = Logger.getLogger( DatabaseManagement.class.getName() );
	
	/**
	 * Method to save database in a file
	 * @param filePath the path of database to save
	 * @param pathFile where the file containing data is store
	 */
	public void saveDatabase(String filePath, Database database) {
		
		try (
				ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filePath))
			) {
			oos.writeObject(database);
			// oos is automatically closed by the try/catch. Because it's open between the try brackets
		} catch (FileNotFoundException e) {
			String msg = "Erreur le programme ne peut pas écrire dans le fichier\"" + filePath + "\""
					+ System.lineSeparator() + "Les données ne seront pas enregistré"
			; 
			JOptionPane.showMessageDialog(null, msg,
					"Sauvegarde des données échouée", JOptionPane.ERROR_MESSAGE)
			;
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Il y a eu une erreur dans l'écriture des données",
					"sauvegarde des données échouée", JOptionPane.ERROR_MESSAGE)
			;
		}	
	}
	
	/**
	 * Method to load data from file
	 * @param filePath filePath where we can find the file containing data 
	 * @return an object DataBase
	 */
	public Database loadDatabase(String filePath) {
		File file = new File(filePath);
		Database database = null;
		try (
				FileInputStream fis = new FileInputStream(file);
				ObjectInputStream ois = new ObjectInputStream(fis)
			) {
			database = (Database) ois.readObject();
			
		} catch (FileNotFoundException e) {
			String msg = "Erreur le programme ne peut pas lire dans le fichier\"" + filePath + "\""
					+ System.lineSeparator() + "Il ne pourra donc pas enregistrer les données"
			;
			JOptionPane.showMessageDialog(null, msg,
					"Recuperation des données echoué", JOptionPane.ERROR_MESSAGE)
			;
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Erreur les données enregistré ne peuvent pas être récuperé",
					"Recuperation des données echoué", JOptionPane.ERROR_MESSAGE)
			;
		} catch (ClassNotFoundException e) {
			LOGGER.log(Level.SEVERE, "Erreur du programmeur, context :", e);
		}
		if (database == null) {
			database = new Database();
		}
		return database;
	}
	
	
}
